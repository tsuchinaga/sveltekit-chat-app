# sveltekit chat app

SvelteKit + socket.io を利用してつくるリアルタイムチャットアプリ

下記ページを参考にさせてもらった  
[Live-Chat with SvelteKit and SocketIO](https://linu.us/live-chat-with-sveltekit-and-socketio)

試しながらのメモは下記  
[Svelte + SvelteKit + WebSocket でチャットアプリ - tsuchinaga](https://scrapbox.io/tsuchinaga/Svelte_+_SvelteKit_+_WebSocket_%E3%81%A7%E3%83%81%E3%83%A3%E3%83%83%E3%83%88%E3%82%A2%E3%83%97%E3%83%AA)
